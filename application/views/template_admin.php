<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Penanggulangan Bencana</title>
	<link rel="shortcut icon" href="<?= config_item('base_url') ?>images/favicon_m3codes.ico?v=1.1"/>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/Ionicons/css/ionicons.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/dist/css/skins/_all-skins.min.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/bootstrap-daterangepicker/daterangepicker.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>asset/select2-master/dist/css/select2.min.css"/>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<?= isset($_styles) ? $_styles : ""; ?>

	<!-- jQuery 3 -->
	<script src="<?= config_item('base_url')?>theme/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="<?= config_item('base_url')?>theme/bower_components/jquery-ui/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?= config_item('base_url')?>theme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script src="<?= config_item('base_url')?>theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?= config_item('base_url')?>theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- Morris.js charts -->
	<script src="<?= config_item('base_url')?>theme/bower_components/raphael/raphael.min.js"></script>
	<script src="<?= config_item('base_url')?>theme/bower_components/morris.js/morris.min.js"></script>
	<!-- Sparkline -->
	<script src="<?= config_item('base_url')?>theme/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="<?= config_item('base_url')?>theme/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
	<!-- daterangepicker -->
	<script src="<?= config_item('base_url')?>theme/bower_components/moment/min/moment.min.js"></script>
	<script src="<?= config_item('base_url')?>theme/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- datepicker -->
	<script src="<?= config_item('base_url')?>theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="<?= config_item('base_url')?>theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<!-- Slimscroll -->
	<script src="<?= config_item('base_url')?>theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="<?= config_item('base_url')?>theme/bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= config_item('base_url')?>theme/dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?= config_item('base_url')?>theme/dist/js/demo.js"></script>
	<script src="<?= config_item('base_url')?>theme/dist/js/jquery.maskMoney.min.js"></script>
	<script src="<?= config_item('base_url')?>asset/select2-master/dist/js/select2.min.js"></script>

	<script src="<?= config_item('base_url')?>asset/tinymce/tinymce.min.js"></script>
	<script src="<?= config_item('base_url')?>asset/js/jquery.numeric.js"></script>
	<script>
		var base_url = "<?= config_item('base_url')?>";
		$(function () {
			$('#example1').DataTable()
			$('#example2').DataTable({
				'paging'      : true,
				'lengthChange': false,
				'searching'   : false,
				'ordering'    : true,
				'info'        : true,
				'autoWidth'   : false
			})
			
			//Date picker
			$('#datepicker').datepicker({
				orientation: "bottom auto",
				autoclose: true,
				format: 'dd-mm-yyyy'
			})
			$('#datepicker_baru').datepicker({
				orientation: "bottom auto",
				autoclose: true,
				format: 'yyyy-mm-dd'
			})
			$('#datepicker2_baru').datepicker({
				orientation: "bottom auto",
				autoclose: true,
				format: 'yyyy-mm-dd'
			})
			$('#datepicker2').datepicker({
				orientation: "bottom auto",
				autoclose: true,
				format: 'dd-mm-yyyy'
			})
			
			$("#kamar").select2({
				placeholder: "Masukan Kamar"
			})

			$("#kamar_order").select2({
				placeholder: "Masukan Kamar"
			})
			
			$("#fasilitas").select2({
				placeholder: "Masukan Fasilitas"
			})
			/* 
			//Date range picker
			$('#reservation').daterangepicker(
			  {
				  locale: {
					format: 'DD/MM/YYYY'
				  },
				  startDate: '<?= date('d/m/Y');?>',
				  endDate: '<?= date('d/m/Y');?>'
			  } 
			)
			 */
			$('.separator_uang').maskMoney({precision:0});

			$('.inputkorban').numeric({ negative: false });
		});
		
		function getkey(e){
			if (window.event)
			return window.event.keyCode;
			else if (e)
			return e.which;
			else
			return null;
		}
		function goodchars(e, goods, field){
			var key, keychar;
			key = getkey(e);
			if (key == null) return true;

				keychar = String.fromCharCode(key);
				keychar = keychar.toLowerCase();
				goods = goods.toLowerCase();

			// check goodkeys
			if (goods.indexOf(keychar) == 1)
				return true;
			// control keys
			if ( key==null || key==0 || key==8 || key==9 || key==27 )
				return true;

			if (key == 13) {
				var i;
				for (i = 0; i < field.form.elements.length; i++)
				if (field == field.form.elements[i])
					break;
				i = (i + 1) % field.form.elements.length;
				field.form.elements[i].focus();
				return false;
			};
			// else return false
			return false;
		}
	</script>
	<?= isset($_scripts) ? $_scripts : ""; ?>
</head>
<?php 
	$url_1 = $this->uri->segment('1');
	$url_2 = $this->uri->segment('2');
?>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="<?= site_url()?>" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>A</b>B</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Aplikasi</b> BPBA</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?= setting()->foto == "" ? "" : config_item('base_url').'img/thumb/'.setting()->foto; ?>" class="user-image" alt="Image">
								<span class="hidden-xs"><?= $this->session->userdata('username') ?></span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
								<img src="<?= setting()->foto == "" ? "" : config_item('base_url').'img/thumb/'.setting()->foto; ?>" class="img-circle" alt="User Image">
								<p>
									<?= $this->session->userdata('username').
									"<small>".$this->session->userdata('nama_lengkap')."</small>"
									?>
								</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="<?= site_url('logins/logout');?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					
						<img src="<?= setting()->foto == "" ? "" : config_item('base_url').'img/'.setting()->foto; ?>" width="100%" alt="User Image">
					
					<?php /*<div class="pull-left info">
						<p><?= $this->session->userdata('username')?></p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>*/ ?>
				</div>
				
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="<?= $url_2 == "" ? "active" : ""; ?>"><a href="<?= site_url('admin');?>"><i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
					<li class="<?= $url_2 == "kabupaten" ? "active" : ""; ?>">
						<a href="<?= site_url('admin/kabupaten');?>"><i class="fa fa-th"></i> <span>Kabupaten</span></a>
					</li>
					<li class="<?= $url_2 == "kecamatan" ? "active" : ""; ?>">
						<a href="<?= site_url('admin/kecamatan');?>"><i class="fa fa-th"></i> <span>Kecamatan</span></a>
					</li>
					<li class="<?= $url_2 == "desa" ? "active" : ""; ?>">
						<a href="<?= site_url('admin/desa');?>"><i class="fa fa-th"></i> <span>Desa</span></a>
					</li>
					<li class="<?= $url_2 == "bencana" ? "active" : ""; ?>">
						<a href="<?= site_url('admin/bencana');?>"><i class="fa fa-bars"></i> <span>Jenis Bencana</span></a>
					</li>
					<li class="<?= $url_2 == "korban" ? "active" : ""; ?>">
						<a href="<?= site_url('admin/korban');?>"><i class="fa fa-bed"></i> <span>Korban</span></a>
					</li>
					<li class="<?= $url_2 == "berita" ? "active" : ""; ?>">
						<a href="<?= site_url('admin/berita');?>"><i class="fa fa-newspaper-o"></i> <span>Berita</span></a>
					</li>

					<li class="treeview<?= $url_2 == "user" || $url_2 == "profil" ? " active" : ""; ?>">
						<a href="#">
							<i class="fa fa-gears"></i> <span>Admin Management</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="<?= $url_2 == "user" ? "active" : ""; ?>"><a href="<?= site_url('admin/user');?>"><i class="fa fa-user"></i> User Admin</a></li>
							<li class="<?= $url_2 == "profil" ? "active" : ""; ?>"><a href="<?= site_url('admin/profil');?>"><i class="fa fa-toggle-right"></i> Profil</a></li>
						</ul>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<?= $contents; ?>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> <?= config_item('versi'); ?>
			</div>
			<strong>Copyright &copy; 2019 <a href="<?= config_item('website'); ?>"><?= config_item('website'); ?></a>.</strong> All rights reserved.
		</footer>
	</div>
<!-- ./wrapper -->
</body>
</html>