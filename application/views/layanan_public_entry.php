<div class="content-wrapper">
	<div class="container">

		<section class="content-header">
			<h1>
				Layanan Publik
				<small><?= setting()->nama; ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="<?= config_item('base_url')?>"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="<?= config_item('base_url').'layanan_publics';?>"> Layanan Publik </a></li>
				<li class="active">Tambah</li>
			</ol>
		</section>

		<section class="content">
			<div class="box box-default">
				<div class="box-body">
					<div class="col-md-12">
						<div class="box box-solid">
							<div class="box-header with-border text-center">
								<h2 class="box-title"><b>Tambah Laporan Bencana</b></h2>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<form class="form-horizontal" id="frmkorban" action="<?=site_url('layanan_publics/proses'); ?>" method="post">
									<input type="hidden" name="korbanid" value="<?= $korbanid ?>">
									<div class="box-body">
										<div class="form-group">
											<label for="kabupaten_id" class="col-sm-2 control-label">Jenis Bencana <font color="red">*</font></label>
											<div class="col-sm-10">
												<?= form_dropdown('bencanaid', $dropdown_bencana, $bencanaid, 'id="bencanaid" class="form-control" required="required"'); ?>
											</div>
										</div>

										<div class="form-group">
											<label for="kabupaten_id" class="col-sm-2 control-label">Kabupaten <font color="red">*</font></label>
											<div class="col-sm-10">
												<?= form_dropdown('kabupatenid', $dropdown_kabupaten, $kabupatenid, 'id="kabupatenid" class="form-control" required="required"'); ?>
											</div>
										</div>

										<div class="form-group">
											<label for="kecamatan_id" class="col-sm-2 control-label">Kecamatan <font color="red">*</font></label>
											<div class="col-sm-10">
												<?= form_dropdown('kecamatanid', $dropdown_kecamatan, $kecamatanid, 'id="kecamatanid" class="form-control" required="required"'); ?>
											</div>
										</div>

										<div class="form-group">
											<label for="kecamatan_id" class="col-sm-2 control-label">Desa <font color="red">*</font></label>
											<div class="col-sm-10">
												<?= form_dropdown('desaid', $dropdown_desa, $desaid, 'id="desaid" class="form-control" required="required"'); ?>
											</div>
										</div>

										<div class="form-group">
											<label for="fasilitas" class="col-sm-2 control-label">Jumlah Korban <font color="red">*</font></label>
											<div class="col-sm-2">
												<input type="text" name="jumlah_korban" id="jumlah_korban" value="<?= $jumlah_korban ?>" class="form-control inputkorban" min="0" max="9999" required="required">
											</div>
											<label class="col-sm-8" style="padding-top: 7px; margin-bottom: 0;text-align: left;">Korban</label>
										</div>

										<div class="form-group">
											<label for="keterangan" class="col-sm-2 control-label">Tanggal</label>
											<div class="col-sm-10">
											<div class="input-group">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control pull-right" id="datepicker_baru" name="tanggal" readonly="readonly" value="<?= $tanggal ?>">
											</div>
											</div>
										</div>

										<div class="form-group">
											<label for="kecamatan_id" class="col-sm-2 control-label">Tahun <font color="red">*</font></label>
											<div class="col-sm-10">
												<?= form_dropdown('tahun', $dropdown_tahun, $tahun, 'id="tahun" class="form-control" required="required"'); ?>
											</div>
										</div>

										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<button type="submit" class="btn btn-info pull-left"><?= $tombol; ?></button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>

			</div>

		</section>

	</div>

</div>