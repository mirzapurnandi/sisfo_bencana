<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Data Kabupaten</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?= site_url('admin/kabupaten')?>">Data Kabupaten</a></li>
			<li class="active"><?= $tombol; ?> Kabupaten</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?= $tombol; ?> Data</h3>
					</div>
					<form class="form-horizontal" id="frmkabupaten" action="<?=site_url('Admin_kabupaten/proses'); ?>" method="post">
						<input type="hidden" name="kabupatenid" value="<?= $kabupatenid ?>">
						<div class="box-body">
							<div class="form-group">
								<label for="fasilitas" class="col-sm-2 control-label">Nama Kabupaten </label>
								<div class="col-sm-10">
									<input type="text" name="nama" id="nama" value="<?= $nama ?>" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-info pull-left kamar"><?= $tombol; ?></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>