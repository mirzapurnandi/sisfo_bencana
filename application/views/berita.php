<div class="content-wrapper">
	<div class="container">

		<section class="content-header">
			<h1>
				Berita
				<small><?= setting()->nama; ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="<?= config_item('base_url')?>"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Berita</li>
			</ol>
		</section>

		<section class="content">
			<div class="box box-default">
				<div class="box-body">
					<div class="col-md-12">
						<div class="box box-solid">
							<div class="box-header with-border text-center">
								<h2 class="box-title"><b>Berita</b></h2>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<?php 
									$no = 0;
									foreach ($result as $key => $val) { 
										$gambar = $val['gambar'] == "" ? config_item('base_url').'img/default.jpg' : config_item('base_url')."img/berita/".$val['gambar'];
										?>
								        <div class="col-lg-3 col-xs-6 text-center">
								          <img class="img-responsive pad" src="<?= $gambar; ?>" alt="Photo">

								          <a href="<?= config_item('base_url').'beritas/'.$val['judul_seo']?>" class="small-box-footer"><?= $val['judul']; ?> <br>Lihat Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
								          <br><br>
								        </div>
								    <?php 
								    $no ++;
								    if($no == 4){
								    	$no = 0;
								    	echo '<div class="clearfix"></div>';
								    }
								} ?>
							      </div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>

			</div>

		</section>

	</div>

</div>