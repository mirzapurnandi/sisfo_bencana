<div class="content-wrapper">
	<div class="container">

		<section class="content-header">
			<h1>
				Berita Detail
				<small><?= setting()->nama; ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="<?= config_item('base_url')?>"><i class="fa fa-dashboard"></i> Home </a></li>
				<li><a href="<?= config_item('base_url').'beritas'; ?>"> Berita </a></li>
				<li class="active">Berita Detail</li>
			</ol>
		</section>

		<section class="content">
			<div class="box box-default">
				<div class="box-body">
					<div class="col-md-12">
						<?php if(count($result) > 0){ ?>
						<div class="box box-solid">
							<div class="box-header with-border text-center">
								<h2 class="box-title"><b><?= $result['judul']; ?></b></h2>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<p><?= str_replace("../../../", config_item('base_url'), $result['deskripsi']); ?></p>
							</div>
							<!-- /.box-body -->
						</div>
						<?php } else {
							echo "<h2 class='text-center'><i> Berita yang anda Cari tidak Ada</i></h2>";
						} ?>
						<!-- /.box -->
					</div>
				</div>

			</div>

		</section>

	</div>

</div>