<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Beritas extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('umum');
		}

		function index(){
			$data['result'] = $this->umum->get_berita();

			$this->template->view('template', 'berita', $data);
		}

		function blog($url = ""){
			$data['result'] = $this->umum->get_berita_url($url);
			$this->template->view('template', 'berita_view', $data);
		}
	}
?>