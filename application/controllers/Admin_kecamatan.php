<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_kecamatan extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session('admin');
			$this->load->model('umum');
		}

		function index(){
			$data['result'] = $this->umum->get_kecamatan();
			$this->template->view('template_admin', 'admin/kecamatan', $data);
		}

		function entry($id = ""){
			$data['dropdown_kabupaten'] = dropdown_kabupaten();
			if($id == ""){
				$data['kecamatanid'] = '';
				$data['kabupatenid'] = '';
				$data['tombol'] 	 = 'Tambah';
				$data['nama'] 		 = '';
			} else {
				$result = $this->umum->get_kecamatan($id);
				$data['kecamatanid'] = $id;
				$data['kabupatenid'] = $result['kab_id'];
				$data['tombol'] 	 = 'Edit';
				$data['nama'] 		 = $result['nmkec'];
			}
			$this->template->view('template_admin', 'admin/kecamatan_entry', $data);
		}

		function proses(){
			$this->db->trans_start();
			$kecamatanid = $this->input->post('kecamatanid') ? $this->input->post('kecamatanid') : "";

			$nama		 = $this->input->post('nama');
			$kabupatenid = $this->input->post('kabupatenid');

			$return_data = array(
				'nmkec'	 => $nama,
				'kab_id' => $kabupatenid
			);
			if($kecamatanid == ""){
				$return_data += array(
					'kdkec' => $this->global_model->getkodeunik("kecamatan", "kdkec", "KEC-")
				);
				$this->global_model->insert('kecamatan', $return_data);
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
			} else {
				$this->global_model->update('kecamatan', $return_data, array('id' => $kecamatanid));
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			}
			redirect('admin/kecamatan');
		}

		function remove($id){
			$this->global_model->delete('kecamatan', array('id' => $id));
			$this->session->set_flashdata('pesan', '<div class="form-group"><div class="col-sm-12 alert alert-error" role="alert">Data Sudah di Hapus</div></div>');
			echo "<script>window.history.go(-1);</script>";
		}
	}
?>