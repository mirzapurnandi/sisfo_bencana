<?php
ob_start();
include('./../index.php');
ob_end_clean();

$delete = true;
$move   = true;
$rename = true;
$disabled = false;

$_CONFIG = array(
    'disabled' => $disabled,
    'uploadURL' => "upload",
    'uploadDir' => "",
    'theme' => "default",

    'types' => array(
        'files'   =>  "",
        'flash'   =>  "swf",
        'images'  =>  "*img",
        'file'    =>  "",
        'media'   =>  "swf flv avi mpg mpeg qt mov wmv asf rm",
        'image'   =>  "*img",
    ),
    'imageDriversPriority' => "imagick gmagick gd",
    'jpegQuality' => 120,
    'thumbsDir' => ".thumbs",
    'maxImageWidth' => 1000,
    'maxImageHeight' => 1000,
    'thumbWidth' => 100,
    'thumbHeight' => 100,
    'watermark' => "pista.id",
    'denyZipDownload' => false,
    'denyUpdateCheck' => false,
    'denyExtensionRename' => false,

    'dirPerms' => 0755,
    'filePerms' => 0644,

    'access' => array(

        'files' => array(
            'upload' => true,
            'delete' => $delete,
            'copy'   => true,
            'move'   => $move,
            'rename' => $rename
        ),

        'dirs' => array(
            'create' => true,
            'delete' => true,
            'rename' => true
        )
    ),

    'deniedExts' => "exe com msi bat cgi pl php phps phtml php3 php4 php5 php6 py pyc pyo pcgi pcgi3 pcgi4 pcgi5 pchi6",
    'filenameChangeChars' => array(),
    'dirnameChangeChars' => array(),
    'mime_magic' => "",
    'cookieDomain' => "",
    'cookiePath' => "",
    'cookiePrefix' => 'KCFINDER_',
    '_normalizeFilenames' => false,
    '_check4htaccess' => true,
    '_sessionVar' => "KCFINDER",
);

?>
