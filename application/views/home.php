<div class="content-wrapper">
	<div class="container">

		<section class="content-header">
			<h1>
				Beranda
				<small><?= setting()->nama; ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="<?= config_item('base_url')?>"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Beranda</li>
			</ol>
		</section>

		<section class="content">
			<div class="box box-default">
				<div class="box-header with-border text-center">
					<h3 class="box-title"><b>Beranda</b></h3>
				</div>
				<div class="box-body">
					<div class="col-md-12">
						<div class="box box-solid">
							<!-- /.box-header -->
							<div class="box-body text-center">
								<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
									<ol class="carousel-indicators">
										<?php for ($i=0; $i < $jumlah; $i++) { ?>
											<li data-target="#carousel-example-generic" data-slide-to="<?=$i?>" class="<?= $i==0 ? 'active' : ''; ?>"></li>
										<?php } ?>
									</ol>
									<div class="carousel-inner">
										<?php foreach ($result as $key => $val) { 
											$gambar = $val['gambar'] == "" ? config_item('base_url').'img/default.jpg' : config_item('base_url')."img/berita/".$val['gambar'];
											?>
											<div class="item <?= $key == 0 ? 'active' : ''; ?>">
												<a href="<?= config_item('base_url').'beritas/'.$val['judul_seo']?>">
												<center><img src="<?= $gambar; ?>" class="img-responsive" alt="<?= $val['judul']; ?>"></center>

												<div class="carousel-caption">
													<?= $val['judul']; ?>
												</div>
												</a>
											</div>
										<?php } ?>
										
									</div>
									<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
										<span class="fa fa-angle-left"></span>
									</a>
									<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
										<span class="fa fa-angle-right"></span>
									</a>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<div class="box box-solid">

							<div class="box-body">
								<div class="row">
									<?php foreach ($result2 as $key2 => $val2) { 
										$gambar2 = $val2['gambar'] == "" ? config_item('base_url').'img/default.jpg' : config_item('base_url')."img/berita/".$val2['gambar'];
										?>
								        <div class="col-lg-3 col-xs-6 text-center">
								          <img class="img-responsive pad" src="<?= $gambar2; ?>" alt="Photo">

								          <a href="<?= config_item('base_url').'beritas/'.$val2['judul_seo']?>" class="small-box-footer"><?= $val2['judul']; ?> <br>Lihat Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
								          <br><br>
								        </div>
								    <?php } ?>
							      </div>
							</div>
						</div>
						<!-- /.box -->
					</div>
				</div>

			</div>

		</section>

	</div>

</div>