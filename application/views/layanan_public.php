<div class="content-wrapper">
	<div class="container">

		<section class="content-header">
			<h1>
				Layanan Publik
				<small><?= setting()->nama; ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="<?= config_item('base_url')?>"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Profil</li>
			</ol>
		</section>

		<section class="content">
			<div class="box box-default">
				<div class="box-body">
					<div class="box-header with-border col-md-2">
						<a href="<?= site_url('layanan_publics/entry')?>" class="btn btn-block btn-primary btn-xs">Tambah Laporan</a>
					</div>
					<div class="box-header with-border text-center col-md-10">
						<h2 class="box-title"><b>Profil Dinas <?= setting()->nama; ?></b></h2>
					</div>
					<div class="clearfix"></div>
					<hr>
					<form class="form-horizontal" id="frmcari" action="<?=site_url('layanan_publics/index/cari'); ?>" method="GET">
						<label class="col-sm-2 control-label">Filter Data </label>
						<div class="col-sm-2">
							<?= form_dropdown('bencanaid', $dropdown_bencana, $bencanaid, 'id="bencanaid" class="form-control"'); ?>
						</div>
						<div class="col-sm-2">
							<?= form_dropdown('kabupatenid', $dropdown_kabupaten, $kabupatenid, 'id="kabupatenid" class="form-control"'); ?>
						</div>
						<div class="col-sm-2">
							<?= form_dropdown('kecamatanid', $dropdown_kecamatan, $kecamatanid, 'id="kecamatanid" class="form-control"'); ?>
						</div>
						<div class="col-sm-2">
							<?= form_dropdown('desaid', $dropdown_desa, $desaid, 'id="desaid" class="form-control"'); ?>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-info pull-left"> Cari </button>
						</div>
					</form>
					<div class="clearfix"></div>
					<hr>

					<div class="col-md-12">
						<div class="box box-solid">
							<!-- /.box-header -->
							<div class="box-body">
								<div class="clearfix"></div>
								<font class="info"><?=$this->session->flashdata('pesan');?></font>
									<table id="example1" class="table table-bordered table-striped">
										<thead>
										<tr>
											<th width="5%">No</th>
											<th width="15%">Jenis Bencana</th>
											<th width="20%">Nama Kabupaten</th>
											<th width="20%">Nama Kecamatan</th>
											<th width="20%">Nama Desa</th>
											<th width="10%">Korban</th>
											<th width="10%">Tahun</th>
										</tr>
										</thead>
										<tbody>
										<?php
										if(count($result) > 0){
											foreach($result as $key => $val){ ?>
												<tr>
													<td><?= $key + 1; ?></td>
													<td><?= $val['nmjenisb'] ?></td>
													<td><?= $val['nmkab'] ?></td>
													<td><?= $val['nmkec'] ?></td>
													<td><?= $val['nmdesa'] ?></td>
													<td><?= $val['korban'] ?></td>
													<td><?= $val['tahun'] ?></td>
												</tr>
												<?php 
											} 
										} ?>
										</tbody>
									</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>

			</div>

		</section>

	</div>

</div>