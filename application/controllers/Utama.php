<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Utama extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('umum');
		}

		function index(){
			$jumlah = 5;
			$data['result'] = $this->umum->get_berita("", $jumlah);
			$data['result2'] = $this->umum->get_berita("", 4);
			$data['jumlah'] = $jumlah;

			$this->template->view('template', 'home', $data);
		}
	}
?>