<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_desa extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session('admin');
			$this->load->model('umum');
		}

		function index(){
			$data['result'] = $this->umum->get_desa();
			$this->template->view('template_admin', 'admin/desa', $data);
		}

		function entry($id = ""){
			$this->template->add_js('asset/js/desa.js?'.config_item('versi'));
			$data['dropdown_kabupaten'] = dropdown_kabupaten();
			if($id == ""){
				$data['dropdown_kecamatan'] = array('== Pilih ==');
				$data['desaid'] 	 = '';
				$data['kecamatanid'] = 0;
				$data['kabupatenid'] = '';
				$data['tombol'] 	 = 'Tambah';
				$data['nama'] 		 = '';
			} else {
				$result = $this->umum->get_desa($id);
				$data['dropdown_kecamatan'] = dropdown_kecamatan($result['kab_id']);
				$data['desaid'] 	 = $id;
				$data['kecamatanid'] = $result['kec_id'];
				$data['kabupatenid'] = $result['kab_id'];
				$data['tombol'] 	 = 'Edit';
				$data['nama'] 		 = $result['nmdesa'];
			}
			$this->template->view('template_admin', 'admin/desa_entry', $data);
		}

		function proses(){
			$this->db->trans_start();
			$desaid = $this->input->post('desaid') ? $this->input->post('desaid') : "";

			$nama		 = $this->input->post('nama');
			$kabupatenid = $this->input->post('kabupatenid');
			$kecamatanid = $this->input->post('kecamatanid');

			$return_data = array(
				'nmdesa' => $nama,
				'kab_id' => $kabupatenid,
				'kec_id' => $kecamatanid
			);
			if($desaid == ""){
				$return_data += array(
					'kddesa' => $this->global_model->getkodeunik("desa", "kddesa", "DESA-")
				);
				$this->global_model->insert('desa', $return_data);
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
			} else {
				$this->global_model->update('desa', $return_data, array('id' => $desaid));
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			}
			redirect('admin/desa');
		}

		function remove($id){
			$this->global_model->delete('desa', array('id' => $id));
			$this->session->set_flashdata('pesan', '<div class="form-group"><div class="col-sm-12 alert alert-error" role="alert">Data Sudah di Hapus</div></div>');
			echo "<script>window.history.go(-1);</script>";
		}
	}
?>