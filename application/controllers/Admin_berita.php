<?php
	if(!defined('BASEPATH'))
		exit('No direct access allowed !');
		
	class Admin_berita extends CI_Controller{
		function __construct(){
			parent::__construct();
			date_default_timezone_set("Asia/Jakarta");
			cek_session('admin');
			$this->load->model('umum');
		}

		function index(){
			$data['result'] = $this->umum->get_berita();

			$this->template->view('template_admin', 'admin/berita', $data);
		}

		function entry($id = ""){
			$this->template->add_js('asset/js/berita.js?'.config_item('versi'));
			if($id == ""){
				$data['beritaid'] 	= '';
				$data['tombol'] 	= 'Tambah';
				$data['judul'] 		= '';
				$data['deskripsi']	= '';
				$data['gambar'] 	= '';
				$data['tanggal'] 	= '';
			} else {
				$result = $this->umum->get_berita($id);
				$data['beritaid'] 	= $id;
				$data['tombol'] 	= 'Edit';
				$data['judul'] 		= $result['judul'];
				$data['deskripsi'] 	= $result['deskripsi'];
				$data['gambar']		= config_item('base_url')."img/berita/".$result['gambar'];
				$data['tanggal'] 	= $result['tanggal'];
			}
			$this->template->view('template_admin', 'admin/berita_entry', $data);
		}
		
		function proses(){
			$beritaid 	 = $this->input->post('beritaid') ? $this->input->post('beritaid') : "";
			$judul		 = $this->input->post('judul');
			$judul_seo	 = seo_title($judul);
			$deskripsi	 = $this->input->post('deskripsi');
			$r_deskripsi = str_replace("../../", "../../../", $deskripsi);

			$insert_data = array('judul' => $judul);

			if($beritaid == ""){
				$cekdata = $this->umum->cek_data('berita', array('judul_seo' => $judul_seo));
				if($cekdata > 0){
					$judul_seo = "duplicate-".$judul_seo;
				}
				$gambar = NULL;
				if($_FILES['gambar']['name'] != ""){
					// Upload Gambar
					$config['upload_path']	 = './img/berita/';
					$config['allowed_types'] = 'gif|GIF|jpg|JPG|png|PNG|jpeg|JPEG';
					$config['file_name']	 = time();
					$this->upload->initialize($config);
					if ($this->upload->do_upload('gambar')){
						$gambar = $this->upload->data('file_name');
						$config2['image_library'] 	= 'gd2';
						$config2['source_image'] 	= $this->upload->upload_path.$gambar;
						$config2['maintain_ratio'] 	= TRUE;
						$config2['width'] 			= 400;
						$config2['height'] 			= 250;
						$config2['new_image'] = './img/berita/thumb/';

						$this->load->library('image_lib', $config2);
						$this->image_lib->resize();
					}
				}
				$insert_data += array(
					'judul_seo'	=> $judul_seo,
					'deskripsi'	=> $r_deskripsi,
					'tanggal'	=> date('Y-m-d H:i:s'),
					'gambar' 	=> $gambar
				);

				$this->global_model->insert('berita', $insert_data);
				$this->session->set_flashdata('pesan', '<div class="alert alert-info" role="alert">Data sudah ditambahkan</div>');
			} else {
				$result = $this->umum->get_berita($beritaid);
				if($_FILES['gambar']['name'] != ""){
					unlink("./img/berita/".$result['gambar']);
					unlink("./img/berita/thumb/".$result['gambar']);

					// Upload Gambar
					$config['upload_path']	 = './img/berita/';
					$config['allowed_types'] = 'gif|GIF|jpg|JPG|png|PNG|jpeg|JPEG';
					$config['file_name']	 = time();
					$this->upload->initialize($config);
					if ($this->upload->do_upload('gambar')){
						$gambar = $this->upload->data('file_name');
						$insert_data += array('gambar' => $gambar);
						$config2['image_library'] 	= 'gd2';
						$config2['source_image'] 	= $this->upload->upload_path.$gambar;
						$config2['maintain_ratio'] 	= TRUE;
						$config2['width'] 			= 400;
						$config2['height'] 			= 250;
						$config2['new_image'] = './img/berita/thumb/';

						$this->load->library('image_lib', $config2);
						$this->image_lib->resize();
					}
				}

				$insert_data += array('deskripsi' => $deskripsi);

				$this->global_model->update('berita', $insert_data, array('id' => $result['id']));
				$this->session->set_flashdata('pesan', '<div class="alert alert-info" role="alert">Data sudah diperbaharui</div>');
			}
			redirect('admin/berita');
		}

		function hapus($id = ""){
			$result = $this->global_model->get('berita', '*', array('id'=>$id), true);
			unlink("./../img/berita/".$result['gambar']);
			unlink("./../img/berita/thumb/".$result['gambar']);
			
			$this->global_model->delete('berita', array('id' => $result['id']));
			$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Dihapus</div>");
			redirect('admin/berita');
		}
	}
?>