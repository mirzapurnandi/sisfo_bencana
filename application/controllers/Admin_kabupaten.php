<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_kabupaten extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session('admin');
			$this->load->model('umum');
		}

		function index(){
			$data['result'] = $this->umum->get_kabupaten();
			$this->template->view('template_admin', 'admin/kabupaten', $data);
		}

		function entry($id = ""){
			if($id == ""){
				$data['kabupatenid'] = '';
				$data['tombol'] 	 = 'Tambah';
				$data['nama'] 		 = '';
			} else {
				$result = $this->umum->get_kabupaten($id);
				$data['kabupatenid'] = $id;
				$data['tombol'] 	 = 'Edit';
				$data['nama'] 		 = $result['nmkab'];
			}
			$this->template->view('template_admin', 'admin/kabupaten_entry', $data);
		}

		function proses(){
			$this->db->trans_start();
			$kabupatenid = $this->input->post('kabupatenid') ? $this->input->post('kabupatenid') : "";

			$nama		 = $this->input->post('nama');

			$return_data = array(
				'nmkab'		=> $nama
			);
			if($kabupatenid == ""){
				$return_data += array(
					'kdkab' => $this->global_model->getkodeunik("kabupaten", "kdkab", "KAB-")
				);
				$this->global_model->insert('kabupaten', $return_data);
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
			} else {
				$this->global_model->update('kabupaten', $return_data, array('id' => $kabupatenid));
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			}
			redirect('admin/kabupaten');
		}

		function remove($id){
			$this->global_model->delete('kabupaten', array('id' => $id));
			$this->session->set_flashdata('pesan', '<div class="form-group"><div class="col-sm-12 alert alert-error" role="alert">Data Sudah di Hapus</div></div>');
			echo "<script>window.history.go(-1);</script>";
		}
	}
?>