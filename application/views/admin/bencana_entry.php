<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Data Jenis Bencana</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?= site_url('admin/bencana')?>">Data Jenis Bencana</a></li>
			<li class="active"><?= $tombol; ?> Jenis Bencana</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?= $tombol; ?> Data</h3>
					</div>
					<form class="form-horizontal" id="frmbencana" action="<?=site_url('Admin_bencana/proses'); ?>" method="post">
						<input type="hidden" name="bencanaid" value="<?= $bencanaid ?>">
						<div class="box-body">
							<div class="form-group">
								<label for="fasilitas" class="col-sm-2 control-label">Nama Bencana </label>
								<div class="col-sm-10">
									<input type="text" name="nama" id="nama" value="<?= $nama ?>" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-info pull-left kamar"><?= $tombol; ?></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>