<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Profils extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('umum');
		}

		function index(){
			$data['result'] = setting();

			$this->template->view('template', 'profil', $data);
		}
	}
?>