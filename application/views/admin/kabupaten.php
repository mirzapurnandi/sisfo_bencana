<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Daftar Kabupaten
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url('admin');?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
			<li>Kabupaten</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<!-- SELECT2 EXAMPLE -->
	<div class="box box-default">
		<div class="box-header with-border col-md-2">
			<a href="<?= site_url('admin/kabupaten/entry')?>" class="btn btn-block btn-primary btn-lg">Tambah</a>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<font class="info"><?=$this->session->flashdata('pesan');?></font>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th width="10%">No</th>
							<th width="75%">Nama Kabupaten</th>
							<th width="15%">Actions</th>
						</tr>
						</thead>
						<tbody>
						<?php
						if(count($result) > 0){
							foreach($result as $key => $val){ ?>
								<tr>
									<td><?= $key + 1; ?></td>
									<td><?= $val['nmkab'] ?></td>
									<td>
										<a href="<?= site_url('admin/kabupaten/entry/'.$val['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
										<a href="<?= site_url('admin_kabupaten/remove/'.$val['id']); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Mau Menghapus Data ini... ?')"><span class="fa fa-trash"></span> Delete</a>
									</td>
								</tr>
								<?php 
							} 
						} ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			Informasi seluruh Daftar Kabupaten.
		</div>
	</div>
	<!-- /.box -->
	</section>
</div>