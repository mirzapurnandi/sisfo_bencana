<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_profil extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session('admin');
			$this->load->model('umum');
		}

		function index(){
			$this->template->add_js('asset/js/berita.js?'.config_item('versi'));
			$result			= $this->global_model->get('company', '*', array('id'=>1), true);
			$data['result'] = $result;
			$this->template->view('template_admin', 'admin/profil', $data);
		}

		function proses(){
			$this->db->trans_start();
			$result			= $this->global_model->get('company', '*', array('id'=>1), true);

			$nama		= $this->input->post('nama');
			$profil	 	= $this->input->post('profil');

			$update_data = array(
				'nama'		=> $nama,
				'profil'	=> $profil
			);

			if($_FILES['gambar']['name'] != ""){
				unlink("./../img/".$result['foto']);
				unlink("./../img/thumb/".$result['foto']);

				// Upload Gambar
				$config['upload_path']	 = './img/';
				$config['allowed_types'] = 'gif|GIF|jpg|JPG|png|PNG|jpeg|JPEG';
				$config['file_name']	 = time();
				$this->upload->initialize($config);
				if ($this->upload->do_upload('gambar')){
					$gambar = $this->upload->data('file_name');
					$update_data += array('foto' => $gambar);
					$config2['image_library'] 	= 'gd2';
					$config2['source_image'] 	= $this->upload->upload_path.$gambar;
					$config2['maintain_ratio'] 	= TRUE;
					$config2['width'] 			= 100;
					$config2['height'] 			= 100;
					$config2['new_image'] = './img/thumb/';

					$this->load->library('image_lib', $config2);
					$this->image_lib->resize();
				}
			}

			$this->global_model->update('company', $update_data, array('id' => 1));
			$this->db->trans_complete();
			$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			redirect('admin/profil');
		}
	}
?>