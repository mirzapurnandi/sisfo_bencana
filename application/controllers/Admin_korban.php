<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_korban extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session('admin');
			$this->load->model('umum');
		}

		function index(){
			$data['result'] = $this->umum->get_korban();
			$this->template->view('template_admin', 'admin/korban', $data);
		}

		function entry($id = ""){
			$this->template->add_js('asset/js/desa.js?'.config_item('versi'));
			$arr_tahun[''] = "== Pilih Tahun ==";
			for($i=date('Y'); $i>=1980; $i--){
				$arr_tahun[$i] = $i;
			}
			$data['dropdown_tahun'] = $arr_tahun;

			$data['dropdown_kabupaten'] = dropdown_kabupaten();
			$data['dropdown_bencana'] 	= dropdown_bencana();
			if($id == ""){
				$data['korbanid'] 	 = '';
				$data['dropdown_kecamatan'] = array('== Pilih Kecamatan ==');
				$data['dropdown_desa'] 		= array('== Pilih Desa ==');
				$data['desaid'] 	 = 0;
				$data['kecamatanid'] = 0;
				$data['kabupatenid'] = 0;
				$data['bencanaid'] 	 = 0;
				$data['tombol'] 	 = 'Tambah';
				$data['jumlah_korban'] = '';
				$data['tanggal'] 	 = date("Y-m-d");
				$data['tahun'] 		 = '';
			} else {
				$data['korbanid'] 	 = $id;
				$result = $this->umum->get_korban($id);

				$data['dropdown_kecamatan'] = dropdown_kecamatan($result['kab_id']);
				$data['dropdown_desa'] 		= dropdown_desa($result['kec_id']);
				$data['desaid'] 	 = $result['desa_id'];
				$data['kecamatanid'] = $result['kec_id'];
				$data['kabupatenid'] = $result['kab_id'];
				$data['bencanaid'] 	 = $result['bencanaid'];
				$data['tombol'] 	 = 'Edit';
				$data['jumlah_korban'] = $result['korban'];
				$data['tanggal'] 	 = $result['tanggal'];
				$data['tahun'] 		 = $result['tahun'];
			}
			$this->template->view('template_admin', 'admin/korban_entry', $data);
		}

		function proses(){
			$this->db->trans_start();
			$korbanid = $this->input->post('korbanid') ? $this->input->post('korbanid') : "";

			$bencanaid		= $this->input->post('bencanaid');
			$jumlah_korban	= $this->input->post('jumlah_korban');
			$tanggal		= $this->input->post('tanggal');
			$tahun			= $this->input->post('tahun');
			$kabupatenid 	= $this->input->post('kabupatenid');
			$kecamatanid 	= $this->input->post('kecamatanid');
			$desaid 		= $this->input->post('desaid');

			$return_data = array(
				'korban' 	=> $jumlah_korban,
				'nmjenisb'	=> $bencanaid,
				'nmkab'		=> $kabupatenid,
				'nmkec'		=> $kecamatanid,
				'nmdesa'	=> $desaid,
				'tanggal'	=> $tanggal,
				'tahun'		=> $tahun
			);
			if($korbanid == ""){
				$return_data += array(
					'kdkorban' => $this->global_model->getkodeunik("korban", "kdkorban", "K-")
				);
				$this->global_model->insert('korban', $return_data);
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
			} else {
				$this->global_model->update('korban', $return_data, array('id' => $korbanid));
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			}
			redirect('admin/korban');
		}

		function remove($id){
			$this->global_model->delete('korban', array('id' => $id));
			$this->session->set_flashdata('pesan', '<div class="form-group"><div class="col-sm-12 alert alert-error" role="alert">Data Sudah di Hapus</div></div>');
			echo "<script>window.history.go(-1);</script>";
		}
	}
?>