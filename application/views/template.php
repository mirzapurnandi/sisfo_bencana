<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Penanggulangan Bencana Aceh</title>
	<link rel="shortcut icon" href="<?= config_item('base_url') ?>images/favicon_m3codes.ico?v=1.1"/>

	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/bootstrap/dist/css/bootstrap.min.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/font-awesome/css/font-awesome.min.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/Ionicons/css/ionicons.min.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/dist/css/AdminLTE.min.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/dist/css/skins/_all-skins.min.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/bootstrap-daterangepicker/daterangepicker.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

	<link rel="stylesheet" href="<?= config_item('base_url')?>asset/select2-master/dist/css/select2.min.css"/>

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<?= isset($_styles) ? $_styles : ""; ?>


	<script src="<?= config_item('base_url')?>theme/bower_components/jquery/dist/jquery.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/bower_components/jquery-ui/jquery-ui.min.js"></script>

	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>

	<script src="<?= config_item('base_url')?>theme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?= config_item('base_url')?>theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/bower_components/raphael/raphael.min.js"></script>
	<script src="<?= config_item('base_url')?>theme/bower_components/morris.js/morris.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/bower_components/moment/min/moment.min.js"></script>
	<script src="<?= config_item('base_url')?>theme/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

	<script src="<?= config_item('base_url')?>theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/bower_components/fastclick/lib/fastclick.js"></script>

	<script src="<?= config_item('base_url')?>theme/dist/js/adminlte.min.js"></script>

	<script src="<?= config_item('base_url')?>theme/dist/js/demo.js"></script>
	<script src="<?= config_item('base_url')?>theme/dist/js/jquery.maskMoney.min.js"></script>
	<script src="<?= config_item('base_url')?>asset/select2-master/dist/js/select2.min.js"></script>
	<script src="<?= config_item('base_url')?>asset/js/jquery.numeric.js"></script>

	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script>
		var base_url = "<?= config_item('base_url')?>";
		$(function () {
			$('#example1').DataTable()
			$('#example2').DataTable({
				'paging'      : true,
				'lengthChange': false,
				'searching'   : false,
				'ordering'    : true,
				'info'        : true,
				'autoWidth'   : false
			})

			//Date picker
			$('#datepicker').datepicker({
				orientation: "bottom auto",
				autoclose: true,
				format: 'dd-mm-yyyy'
			})
			$('#datepicker_baru').datepicker({
				orientation: "bottom auto",
				autoclose: true,
				format: 'yyyy-mm-dd'
			})
			$('#datepicker2_baru').datepicker({
				orientation: "bottom auto",
				autoclose: true,
				format: 'yyyy-mm-dd'
			})
			$('#datepicker2').datepicker({
				orientation: "bottom auto",
				autoclose: true,
				format: 'dd-mm-yyyy'
			})
			
			$("#kamar").select2({
				placeholder: "Masukan Kamar"
			})

			$("#kamar_order").select2({
				placeholder: "Masukan Kamar"
			})
			
			$("#fasilitas").select2({
				placeholder: "Masukan Fasilitas"
			})

			$('.separator_uang').maskMoney({precision:0});

			$('.inputkorban').numeric({ negative: false });
		});
	</script>
	<?= isset($_scripts) ? $_scripts : ""; ?>
</head>
<?php 
$url_1 = $this->uri->segment('1');
?>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?= config_item('base_url')?>" class="navbar-brand"><b>BPBA</b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="<?= $url_1 == "" ? "active" : ""; ?>"><a href="<?= config_item('base_url')?>">Beranda</a></li>
            <li class="<?= $url_1 == "profil" ? "active" : ""; ?>"><a href="<?= config_item('base_url').'profil'?>">Profil</a></li>
            <li class="<?= $url_1 == "layanan_publics" ? "active" : ""; ?>"><a href="<?= config_item('base_url').'layanan_publics'?>">Layanan Publik</a></li>
            <li class="<?= $url_1 == "grafiks" ? "active" : ""; ?>"><a href="<?= config_item('base_url').'grafiks'?>">Grafik Bencana</a></li>
            <li class="<?= $url_1 == "beritas" ? "active" : ""; ?>"><a href="<?= config_item('base_url').'beritas'?>">Berita</a></li>
 
          </ul>
        </div>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
          	<?php if($this->session->userdata('userid') != ""){ ?>
            <li class="dropdown user user-menu">

              <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                <img src="<?= setting()->foto == "" ? "" : config_item('base_url').'img/thumb/'.setting()->foto; ?>" class="user-image" alt="User Image">

                <span class="hidden-xs"><?= $this->session->userdata('username') ?></span>
              </a>
              <ul class="dropdown-menu">

                <li class="user-header">
                  <img src="<?= setting()->foto == "" ? "" : config_item('base_url').'img/'.setting()->foto; ?>" class="img-circle" alt="User Image">

                  <p>
                    <?= $this->session->userdata('username')."<small>".$this->session->userdata('nama_lengkap')."</small>" ?>
                  </p>
                </li>

                <li class="user-footer">
                  <div class="pull-left">
                    <a href="<?= config_item('base_url').'admin'; ?>" class="btn btn-default btn-flat">Halaman Admin</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?= config_item('base_url').'logins/logout'; ?>" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
        	<?php } else {
        		echo '<li><a href="'.config_item('base_url').'logins"> Login </a></li>';
        	} ?>
          </ul>
        </div>

      </div>

    </nav>
  </header>

  <?= $contents; ?>

  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> <?= config_item('versi'); ?>
      </div>
      <strong>Copyright &copy; 2019 <a href="<?= config_item('website'); ?>"><?= config_item('website'); ?></a>.</strong> All rights reserved.
    </div>

  </footer>
</div>
</body>
</html>
