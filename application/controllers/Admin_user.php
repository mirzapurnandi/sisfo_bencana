<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_user extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session('admin');
			$this->load->model('umum');
		}

		function index(){
			$data['result'] = $this->umum->get_user();
			$this->template->view('template_admin', 'admin/user', $data);
		}

		function entry($id = ""){
			if($id == ""){
				$data['userid'] 	= '';
				$data['tombol'] 	= 'Tambah';
				$data['nama'] 		= '';
				$data['username']	= '';
				$data['password'] 	= '';
			} else {
				$result = $this->umum->get_user($id);
				$data['userid'] 	= $id;
				$data['tombol'] 	= 'Edit';
				$data['nama'] 		= $result['nama'];
				$data['username']	= $result['username'];
				$data['password'] = $result['password_hid'];
			}
			$this->template->view('template_admin', 'admin/user_entry', $data);
		}

		function proses(){
			$this->db->trans_start();
			$userid = $this->input->post('userid') ? $this->input->post('userid') : "";

			$nama		 = $this->input->post('nama');
			$username	 = $this->input->post('username');
			$password	 = $this->input->post('password');

			$return_data = array(
				'nama'		=> $nama,
				'username'	=> $username,
				'password'	=> MD5($password),
				'level'		=> 'admin'
			);
			if($userid == ""){
				$return_data += array(
					'created'	=> date("Y-m-d H:i:s", time())
				);
				if($this->umum->find_user($username) > 0){
					$this->session->set_flashdata('pesan', "<div class='alert alert-danger' role='alert'>Username Sudah digunakan</div>");
				} else {
					$this->global_model->insert('user', $return_data);
					$this->db->trans_complete();
					$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
				}
			} else {
				if($this->umum->find_user($username, $userid) > 0){
					$this->session->set_flashdata('pesan', "<div class='alert alert-danger' role='alert'>Username Sudah digunakan</div>");
				} else {
					$this->global_model->update('user', $return_data, array('id' => $userid));
					$this->db->trans_complete();
					$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
				}
			}
			redirect('admin/user');
		}

		function remove($id){
			$this->global_model->delete('user', array('id' => $id));
			$this->session->set_flashdata('pesan', '<div class="form-group"><div class="col-sm-12 alert alert-error" role="alert">Data Sudah di Hapus</div></div>');
			echo "<script>window.history.go(-1);</script>";
		}
	}
?>