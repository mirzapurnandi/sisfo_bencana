<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Data Admin</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?= site_url('admin/kabupaten')?>">Data Admin</a></li>
			<li class="active"><?= $tombol; ?> Admin</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?= $tombol; ?> Data</h3>
					</div>
					<form class="form-horizontal" id="frmuser" action="<?=site_url('Admin_user/proses'); ?>" method="post">
						<input type="hidden" name="userid" value="<?= $userid ?>">
						<div class="box-body">
							<div class="form-group">
								<label for="nama" class="col-sm-2 control-label">Nama </label>
								<div class="col-sm-10">
									<input type="text" name="nama" id="nama" value="<?= $nama ?>" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<label for="username" class="col-sm-2 control-label">Username </label>
								<div class="col-sm-10">
									<input type="text" name="username" id="username" value="<?= $username ?>" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<label for="password" class="col-sm-2 control-label">Password </label>
								<div class="col-sm-10">
									<input type="text" name="password" id="password" value="<?= $password ?>" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-info pull-left kamar"><?= $tombol; ?></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>