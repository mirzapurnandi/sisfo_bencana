$(document).ready(function(){
	$("#kabupatenid").change(function(){
	    var kabupatenid = $(this).val();
	    if(kabupatenid == ""){
	        $("#kecamatanid").html("== Pilih Kecamatan ==");
	    }
	    $.ajax({
	        url: base_url + "ajax_function/cari_kecamatan",
	        type: "POST",
	        data: { kabupatenid : kabupatenid },
	        dataType: "html",
	        cache: false,
	        success: function(response){
	            $("#kecamatanid").html(response);
	        }
	    });
	});

$("#kecamatanid").change(function(){
    var kecamatanid = $(this).val();
    if(kecamatanid == ""){
        $("#desaid").html("== Pilih Desa ==");
    }
    $.ajax({
        url: base_url + "ajax_function/cari_desa",
        type: "POST",
        data: { kecamatanid : kecamatanid },
        dataType: "html",
        cache: false,
        success: function(response){
            $("#desaid").html(response);
        }
    });
});
});