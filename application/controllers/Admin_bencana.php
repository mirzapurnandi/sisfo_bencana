<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_bencana extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session('admin');
			$this->load->model('umum');
		}

		function index(){
			$data['result'] = $this->umum->get_bencana();
			$this->template->view('template_admin', 'admin/bencana', $data);
		}

		function entry($id = ""){
			if($id == ""){
				$data['bencanaid'] 	 = '';
				$data['tombol'] 	 = 'Tambah';
				$data['nama'] 		 = '';
			} else {
				$result = $this->umum->get_bencana($id);
				$data['bencanaid'] 	 = $id;
				$data['tombol'] 	 = 'Edit';
				$data['nama'] 		 = $result['nmjenisb'];
			}
			$this->template->view('template_admin', 'admin/bencana_entry', $data);
		}

		function proses(){
			$this->db->trans_start();
			$bencanaid = $this->input->post('bencanaid') ? $this->input->post('bencanaid') : "";

			$nama		 = $this->input->post('nama');

			$return_data = array(
				'nmjenisb' => $nama
			);
			if($bencanaid == ""){
				$return_data += array(
					'kdjenisb' => $this->global_model->getkodeunik("jenis_bencana", "kdjenisb", "JB-")
				);
				$this->global_model->insert('jenis_bencana', $return_data);
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
			} else {
				$this->global_model->update('jenis_bencana', $return_data, array('id' => $bencanaid));
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			}
			redirect('admin/bencana');
		}

		function remove($id){
			$this->global_model->delete('jenis_bencana', array('id' => $id));
			$this->session->set_flashdata('pesan', '<div class="form-group"><div class="col-sm-12 alert alert-error" role="alert">Data Sudah di Hapus</div></div>');
			echo "<script>window.history.go(-1);</script>";
		}
	}
?>