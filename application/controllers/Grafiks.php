<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Grafiks extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('umum');
		}

		function index(){
			$result_tahun = $this->umum->get_group_tahun();
			$get_grafik = $this->umum->get_grafik();

			$this->template->add_js("
				$(document).ready(function(){
					Highcharts.chart('grafik_bencana', {
					    chart: {
					        type: 'column'
					    },
					    title: {
					        text: 'Grapik Bencana'
					    },
					    xAxis: {
					        categories: [".$result_tahun."]
					    },

					    yAxis: {
					        allowDecimals: false,
					        min: 0,
					        title: {
					            text: 'Total Kejadian'
					        }
					    },

					    tooltip: {
					        formatter: function () {
					            return '<b>' + this.x + '</b><br/>' +
					                this.series.name + ': ' + this.y + '<br/>' +
					                'Total Kejadian: ' + this.point.stackTotal;
					        }
					    },

					    plotOptions: {
					        column: {
					            stacking: 'normal'
					        }
					    },

					    series: [
					    	".$get_grafik."
					    ]
					});
				});
			", 'embed');

			$data['result'] = $this->umum->get_berita();

			$this->template->view('template', 'grafik', $data);
		}
	}
?>