<div class="content-wrapper">
	<section class="content-header">
		<h1>Data Profil</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Profil</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Edit Data</h3>
						<font class="info"><?=$this->session->flashdata('pesan');?></font>
					</div>
					<form class="form-horizontal" id="frmberita" action="<?=site_url('Admin_profil/proses'); ?>" method="post" enctype="multipart/form-data">
						<div class="box-body">
							<div class="form-group">
								<label for="fasilitas" class="col-sm-2 control-label">Nama </label>
								<div class="col-sm-10">
									<input type="text" name="nama" id="nama" value="<?= $result['nama'] ?>" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<label for="fasilitas" class="col-sm-2 control-label">Profil </label>
								<div class="col-sm-10">
									<textarea name="profil" id="editor1" cols="50" rows="15" class="form-control textarea" placeholder="Profil Aplikasi..."><?= $result['profil'] ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label for="focusedinput" class="col-sm-2 control-label">Gambar </label>
								<div class="col-sm-5">
									<input type="file" name="gambar" id="gambar" onchange="tampilkanPreview(this,'preview')"/>
									<span class="text-danger bigger-110 red"> Ukuran Gambar <b>500 x 500 px / 1000 x 1000 px</b></span>
								</div>
								<div class="col-sm-5">
									<img id="preview" src="<?= $result['foto'] == '' ? '' : config_item('base_url').'img/'.$result['foto']; ?>" alt="" width="90%" class="img-responsive" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-info pull-left">Perbaharui</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>