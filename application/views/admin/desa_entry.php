<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Data Desa</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?= site_url('admin/kabupaten')?>">Data Desa</a></li>
			<li class="active"><?= $tombol; ?> Desa</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?= $tombol; ?> Data</h3>
					</div>
					<form class="form-horizontal" id="frmdesa" action="<?=site_url('Admin_desa/proses'); ?>" method="post">
						<input type="hidden" name="desaid" value="<?= $desaid ?>">
						<div class="box-body">
							<div class="form-group">
								<label for="kabupaten_id" class="col-sm-2 control-label">Kabupaten <font color="red">*</font></label>
								<div class="col-sm-10">
									<?= form_dropdown('kabupatenid', $dropdown_kabupaten, $kabupatenid, 'id="kabupatenid" class="form-control" required="required"'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="kecamatan_id" class="col-sm-2 control-label">Kecamatan <font color="red">*</font></label>
								<div class="col-sm-10">
									<?= form_dropdown('kecamatanid', $dropdown_kecamatan, $kecamatanid, 'id="kecamatanid" class="form-control" required="required"'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="fasilitas" class="col-sm-2 control-label">Nama Desa <font color="red">*</font></label>
								<div class="col-sm-10">
									<input type="text" name="nama" id="nama" value="<?= $nama ?>" class="form-control" required="required">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-info pull-left kamar"><?= $tombol; ?></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>