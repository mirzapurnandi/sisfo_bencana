<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin_utama extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session('admin');
			//$this->load->model('umum');
		}

		function index(){
			$data = array();

			$this->template->view('template_admin', 'admin/beranda', $data);
		}
	}
?>