<div class="content-wrapper">
	<div class="container">

		<section class="content-header">
			<h1>
				Profil
				<small><?= setting()->nama; ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="<?= config_item('base_url')?>"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Profil</li>
			</ol>
		</section>

		<section class="content">
			<div class="box box-default">
				<div class="box-body">
					<div class="col-md-12">
						<div class="box box-solid">
							<div class="box-header with-border text-center">
								<h2 class="box-title"><b>Profil Dinas <?= setting()->nama; ?></b></h2>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<p><?= str_replace("../", config_item('base_url'), $result->profil); ?></p>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>

			</div>

		</section>

	</div>

</div>