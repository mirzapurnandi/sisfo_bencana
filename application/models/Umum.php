<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Umum extends CI_Model {

	function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	function get_kabupaten($id = ""){
		$this->db->select('
			kabupaten.*
		');
		$this->db->from('kabupaten');
		if($id != ""){
			$this->db->where('kabupaten.id', $id);
		}
		$this->db->order_by('kabupaten.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function get_kecamatan($id = ""){
		$this->db->select('
			kecamatan.*,
			kabupaten.nmkab as nmkab
		');
		$this->db->from('kecamatan');
		$this->db->join('kabupaten', 'kabupaten.id = kecamatan.kab_id', 'left');
		if($id != ""){
			$this->db->where('kecamatan.id', $id);
		}
		$this->db->order_by('kecamatan.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function get_desa($id = ""){
		$this->db->select('
			desa.*,
			(select nmkab from kabupaten where kabupaten.id = desa.kab_id) as nmkab,
			(select nmkec from kecamatan where kecamatan.id = desa.kec_id) as nmkec
		');
		$this->db->from('desa');
		if($id != ""){
			$this->db->where('desa.id', $id);
		}
		$this->db->order_by('desa.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function get_bencana($id = ""){
		$this->db->select('
			jenis_bencana.*
		');
		$this->db->from('jenis_bencana');
		if($id != ""){
			$this->db->where('jenis_bencana.id', $id);
		}
		$this->db->order_by('jenis_bencana.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function get_berita($id = "", $limit = ""){
		$this->db->select('
			berita.*
		');
		$this->db->from('berita');
		if($id != ""){
			$this->db->where('berita.id', $id);
		}
		if($limit != ""){
			$this->db->limit($limit);
		}
		$this->db->order_by('berita.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function get_berita_url($url){
		$this->db->select('
			berita.*
		');
		$this->db->from('berita');
		$this->db->where('berita.judul_seo', $url);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row_array();
		} else {
			return NULL;
		}
	}

	function get_user($id = ""){
		$this->db->select('
			user.*
		');
		$this->db->from('user');
		if($id != ""){
			$this->db->where('user.id', $id);
		}
		$this->db->order_by('user.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function find_user($username, $id = ""){
		$this->db->select('
			user.*
		');
		$this->db->from('user');
		$this->db->where('user.username', $username);

		if($id != ""){
			$this->db->where_not_in('user.id', $id);
		}

		$query = $this->db->get();
		if($query->num_rows() > 0){
			return 1;
		} else {
			return 0;
		}
	}

	function cek_data($table, $where = "", $id = ""){
		$this->db->select('
			*
		');
		$this->db->from($table);
		if($where != '') {
			if(is_array($where)) {
				foreach ($where as $key => $val) {
					if(is_array($val)) {
						$this->db->where_in($key, $val);
					} else {
						$this->db->where($key, $val);
					}
				}
			} else {
				$this->db->where($where);
			}
		}

		if($id != ""){
			$this->db->where_not_in('id', $id);
		}
		
		$data = $this->db->get();
		if($data->num_rows() > 0){
			return count($data->result_array());
		} else {
			return 0;
		}
	}

	function get_korban($id = "", $bencanaid = "", $kabupatenid = "", $kecamatanid = 0, $desaid = 0){
		$this->db->select('
			korban.id,
			korban.kdkorban,
			korban.nmkab as kab_id,
			korban.nmkec as kec_id,
			korban.nmdesa as desa_id,
			korban.nmjenisb as bencanaid,
			korban.korban,
			korban.tanggal,
			korban.tahun,
			jenis_bencana.nmjenisb,
			(select nmkab from kabupaten where kabupaten.id = korban.nmkab) as nmkab,
			(select nmkec from kecamatan where kecamatan.id = korban.nmkec) as nmkec,
			(select nmdesa from desa where desa.id = korban.nmdesa) as nmdesa
		');
		$this->db->from('korban');
		$this->db->join('jenis_bencana', 'jenis_bencana.id = korban.nmjenisb', 'left');
		if($id != ""){
			$this->db->where('korban.id', $id);
		}

		if($bencanaid != ""){
			$this->db->where('korban.nmjenisb', $bencanaid);
		}

		if($kabupatenid != ""){
			$this->db->where('korban.nmkab', $kabupatenid);
		}

		if($kecamatanid > 0){
			$this->db->where('korban.nmkec', $kecamatanid);
		}

		if($desaid > 0){
			$this->db->where('korban.nmdesa', $desaid);
		}

		$this->db->order_by('korban.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function get_group_tahun($group_by = ""){
		$this->db->select('korban.tahun');
		$this->db->from('korban');
		$this->db->group_by('korban.tahun');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$data = '';
			$arr_data = array();
			$count = count($query->result());
			foreach ($query->result_array() as $key => $val) {
				$arr_data[$key] = $val['tahun'];
				$data .= $val['tahun'];
				if($count > ($key + 1)){
					$data .= ', ';
				}
			}
			if($group_by == ""){
				return $data;
			} else {
				return $arr_data;
			}
		} else {
			return NULL;
		}
	}

	function cari_data($bencanaid = "", $tahun = ""){
		$this->db->select('*');
		$this->db->from('korban');
		$this->db->where('korban.nmjenisb', $bencanaid);
		$this->db->where('korban.tahun', $tahun);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return count($query->result());
		} else {
			return 0;
		}
	}

	function get_grafik(){
		$bencana = $this->get_bencana();
		$count = count($bencana);

		$data = '';
		foreach ($bencana as $key => $val) {

			$get_group_tahun = $this->get_group_tahun('no');
			$count_data = count($get_group_tahun);
			$panggil_data = '';
			foreach ($get_group_tahun as $key2 => $val2) {
				$panggil_data .= $this->cari_data($val['id'], $val2);
				if($count_data > ($key2 + 1)){
					$panggil_data .= ', ';
				}
			}

			$data .= "{ name: '".$val['nmjenisb']."', data: [".$panggil_data."]}";
			if($count > ($key + 1)){
				$data .= ', ';
			}
		}

		return $data;
	}
}
?>
