<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 	= 'utama';
$route['404_override'] 			= '';
$route['admin'] 		 		= 'admin_utama/index';

## ========= Kabupaten ========= ##
$route['admin/kabupaten'] 				= 'admin_kabupaten/index'; 
$route['admin/kabupaten/entry'] 		= 'admin_kabupaten/entry'; 
$route['admin/kabupaten/entry/(:num)'] 	= 'admin_kabupaten/entry/$1'; 
## ========= Akhir Kabupaten ========= ##

## ========= Kecamatan ========= ##
$route['admin/kecamatan'] 				= 'admin_kecamatan/index'; 
$route['admin/kecamatan/entry'] 		= 'admin_kecamatan/entry'; 
$route['admin/kecamatan/entry/(:num)'] 	= 'admin_kecamatan/entry/$1'; 
## ========= Akhir Kecamatan ========= ##

## ========= Desa ========= ##
$route['admin/desa'] 				= 'admin_desa/index'; 
$route['admin/desa/entry'] 			= 'admin_desa/entry'; 
$route['admin/desa/entry/(:num)'] 	= 'admin_desa/entry/$1'; 
## ========= Akhir Desa ========= ##

## ========= Bencana ========= ##
$route['admin/bencana'] 				= 'admin_bencana/index'; 
$route['admin/bencana/entry'] 			= 'admin_bencana/entry'; 
$route['admin/bencana/entry/(:num)'] 	= 'admin_bencana/entry/$1'; 
## ========= Akhir Desa ========= ##

## ========= User ========= ##
$route['admin/berita'] 				= 'admin_berita/index'; 
$route['admin/berita/entry'] 		= 'admin_berita/entry'; 
$route['admin/berita/entry/(:num)'] = 'admin_berita/entry/$1'; 
## ========= Akhir User ========= ##

## ========= User ========= ##
$route['admin/user'] 				= 'admin_user/index'; 
$route['admin/user/entry'] 			= 'admin_user/entry'; 
$route['admin/user/entry/(:num)'] 	= 'admin_user/entry/$1'; 
## ========= Akhir User ========= ##

## ========= Korban ========= ##
$route['admin/korban'] 				= 'admin_korban/index'; 
$route['admin/korban/entry'] 		= 'admin_korban/entry'; 
$route['admin/korban/entry/(:num)'] = 'admin_korban/entry/$1'; 
## ========= Akhir User ========= ##

$route['admin/profil'] 				= 'admin_profil/index'; 

$route['profil']	 				= 'profils/index';
$route['beritas/(:any)']	 		= 'beritas/blog/$1';

$route['translate_uri_dashes'] 	= FALSE;
