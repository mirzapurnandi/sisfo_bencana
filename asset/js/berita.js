tinyMCE.init({
		selector: ".textarea",
		plugins: [
					"advlist autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
				],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	file_browser_callback:function(d,a,b,c){
		tinyMCE.activeEditor.windowManager.open({
			file:base_url+"asset/kcfinder/browse.php?opener=tinymce4&field="+d+"&type="+b+"&dir="+b+"/public",title:"KCFinder web file manager",
			width:700,
			height:500,
			inline:true,
			close_previous:false
		},
		{ 
			window:c,input:d
		});
		return false
	}
});

function tampilkanPreview(foto,idpreview){ //membuat objek gambar
	var gb = foto.files;
	//loop untuk merender gambar
	for (var i = 0; i < gb.length; i++)
	{ //bikin variabel
		var gbPreview = gb[i];
		var imageType = /image.*/;
		var preview=document.getElementById(idpreview);
		var reader = new FileReader();
		if (gbPreview.type.match(imageType))
		{ //jika tipe data sesuai
			preview.file = gbPreview;
			reader.onload = (function(element)
			{
				return function(e)
				{
					element.src = e.target.result;
				};
			})(preview);
			//membaca data URL gambar
			reader.readAsDataURL(gbPreview);
		}
		else
		{ //jika tipe data tidak sesuai
			alert("Tipe file tidak sesuai. Gambar harus bertipe .png, .gif atau .jpg.");
		}
	}
}
/* 
$(document).ready(function(){
	$(".btn_simpan").click(function(){
		$(".btn_simpan").attr("disabled", true);
		$(".btn_simpan").html("Sedang Proses...");
		// validasi semua form
		if($("#judul").val() == "") {
			alert("Silahkah isi Judul");
			$("#judul").focus();
			$(".btn_simpan").removeAttr("disabled");
			$(".btn_simpan").html("Simpan");
			return false;
		} else {
			$("#frmpage").submit();
		}
	});
});*/