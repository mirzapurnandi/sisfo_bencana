<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

if (!function_exists('dropdown_bencana')) {
	function dropdown_bencana(){
		$CI = &get_instance();
		$CI->load->database();
		## Menampilkan data
		$CI->db->select('*');
		$CI->db->from('jenis_bencana');
		$CI->db->order_by('id', 'asc');
		$hasil = $CI->db->get();

		$arr_data[''] = "== Pilih Jenis Bencana ==";
		if($hasil->num_rows()>0) {
			foreach($hasil->result_array() as $key => $val){
				$arr_data[$val['id']] = $val['nmjenisb'];
			}
		}
		return $arr_data;
	}
}

if (!function_exists('dropdown_kabupaten')) {
	function dropdown_kabupaten(){
		$CI = &get_instance();
		$CI->load->database();
		## Menampilkan data
		$CI->db->select('*');
		$CI->db->from('kabupaten');
		$CI->db->order_by('id', 'asc');
		$hasil = $CI->db->get();

		$arr_data[''] = "== Pilih Kabupaten ==";
		if($hasil->num_rows()>0) {
			foreach($hasil->result_array() as $key => $val){
				$arr_data[$val['id']] = $val['nmkab'];
			}
		}
		return $arr_data;
	}
}

if (!function_exists('dropdown_kecamatan')) {
	function dropdown_kecamatan($kabupaten = 0){
		$CI = &get_instance();
		$CI->load->database();
		## Menampilkan data Kecamatan
		$CI->db->select('*');
		$CI->db->from('kecamatan');
		$CI->db->order_by('id', 'asc');
		if($kabupaten > 0){
			$CI->db->where('kab_id', $kabupaten);
		}
		
		$hasil = $CI->db->get();

		$arr_kab[''] = "== Pilih Kecamatan ==";
		if($hasil->num_rows()>0) {
			foreach($hasil->result_array() as $key => $val){
				$arr_kab[$val['id']] = $val['nmkec'];
			}
		}
		return ($arr_kab);
	}
}

if (!function_exists('dropdown_desa')) {
	function dropdown_desa($kecamatan = 0){
		$CI = &get_instance();
		$CI->load->database();
		## Menampilkan data Desa
		$CI->db->select('*');
		$CI->db->from('desa');
		$CI->db->order_by('id', 'asc');
		if($kecamatan > 0){
			$CI->db->where('kec_id', $kecamatan);
		}
		
		$hasil = $CI->db->get();

		$arr_desa[''] = "== Pilih Desa ==";
		if($hasil->num_rows()>0) {
			foreach($hasil->result_array() as $key => $val){
				$arr_desa[$val['id']] = $val['nmdesa'];
			}
		}
		return ($arr_desa);
	}
}

?>
