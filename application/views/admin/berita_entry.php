<div class="content-wrapper">
	<section class="content-header">
		<h1>Data Berita</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?= site_url('admin/berita')?>">Data Berita</a></li>
			<li class="active"><?= $tombol; ?> Berita</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?= $tombol; ?> Data</h3>
					</div>
					<form class="form-horizontal" id="frmberita" action="<?=site_url('Admin_berita/proses'); ?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="beritaid" value="<?= $beritaid ?>">
						<div class="box-body">
							<div class="form-group">
								<label for="fasilitas" class="col-sm-2 control-label">Judul </label>
								<div class="col-sm-10">
									<input type="text" name="judul" id="judul" value="<?= $judul ?>" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<label for="fasilitas" class="col-sm-2 control-label">Deskripsi </label>
								<div class="col-sm-10">
									<textarea name="deskripsi" id="editor1" cols="50" rows="15" class="form-control textarea" placeholder="Deskripsi Berita..."><?= $deskripsi ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label for="focusedinput" class="col-sm-2 control-label">Gambar </label>
								<div class="col-sm-5">
									<input type="file" name="gambar" id="gambar" onchange="tampilkanPreview(this,'preview')"/>
									<span class="text-danger bigger-110 red"> Ukuran Gambar <b>800 x 500 px / 1200 x 750 px</b></span>
								</div>
								<div class="col-sm-5">
									<img id="preview" src="<?= $gambar; ?>" alt="" width="90%" class="img-responsive" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-info pull-left"><?= $tombol; ?></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>