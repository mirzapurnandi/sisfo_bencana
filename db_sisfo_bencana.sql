-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 12 Sep 2019 pada 10.53
-- Versi Server: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_sisfo_bencana`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
`id` int(11) NOT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `judul_seo` varchar(200) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id`, `judul`, `judul_seo`, `deskripsi`, `gambar`, `tanggal`) VALUES
(1, 'Postingan Pertama', 'postingan-pertama', '<p style="text-align: justify;">sadkajs uasdhf vjkdzhvuaszdv zksdfjv zkjsdjv zsildvi. dsifh sdifhsdivh zsoidvb dzfv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;</p>\r\n<p><img src="../../../asset/kcfinder/upload/image/test.png" alt="asfdv sfds" width="99%" /></p>', '1568132012.png', '2019-09-10 23:13:32'),
(2, 'berita kedua ss', 'berita-kedua-ss', '<p style="text-align: justify;">sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;</p>\r\n<p style="text-align: justify;">sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;sdfsd sjdhfgv dsjkhf vjkdshz fvkjzhd jkdfhv djzkxhv jdfhzv dfv.&nbsp;</p>\r\n<p style="text-align: justify;">asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.</p>', '1568256410.png', '2019-09-12 09:46:50'),
(3, 'Berita Ketiga', 'berita-ketiga', '<p style="text-align: justify;">asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;</p>\r\n<p style="text-align: justify;">asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;</p>\r\n<p style="text-align: justify;">asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;asasavds hrgfwe sdkhfweg sdhgds kjh kujsdfsd.&nbsp;</p>', '1568256448.png', '2019-09-12 09:47:28'),
(4, 'Berita Keempat', 'berita-keempat', '<p style="text-align: justify;">dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;</p>\r\n<p style="text-align: justify;">dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;</p>\r\n<p style="text-align: justify;">dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;</p>\r\n<p style="text-align: justify;">dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;dajsj fsjhd fukajshf dvkjzhd vkzjsdufjb vkdjzfvb sduzkulxvjb szdilolfvn kjzs,hmd vdzv.&nbsp;</p>', '1568256491.png', '2019-09-12 09:48:11'),
(5, 'Berita Kelima', 'berita-kelima', '<p style="text-align: justify;">sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;</p>\r\n<p style="text-align: justify;">sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;</p>\r\n<p style="text-align: justify;">sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;sdfkhbskdjgljsz sdilfukvajds zvcjksdlzikvn skjzdifx.kvn,m fdhzkjx,fivawksdn zx.&nbsp;</p>', '1568256551.png', '2019-09-12 09:49:12'),
(6, 'Berita Keenam', 'berita-keenam', '<p style="text-align: justify;">klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd. woepflskdbgfilsdkjfvskld.fbk.&nbsp;klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd. woepflskdbgfilsdkjfvskld.fbk.&nbsp;klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd. woepflskdbgfilsdkjfvskld.fbk.&nbsp;klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd. woepflskdbgfilsdkjfvskld.fbk.&nbsp;</p>\r\n<p style="text-align: justify;">klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd. woepflskdbgfilsdkjfvskld.fbk.&nbsp;klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd. woepflskdbgfilsdkjfvskld.fbk.&nbsp;</p>\r\n<p style="text-align: justify;">klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd. woepflskdbgfilsdkjfvskld.fbk.&nbsp;klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd. woepflskdbgfilsdkjfvskld.fbk.&nbsp;klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd. woepflskdbgfilsdkjfvskld.fbk.&nbsp;klsdjfkl,sm dlskjd fblkes/.rkdgf ck,.skdrldg vlksdf.zj,xcvm dilzkxjfv sdzkcj cxvdfxcvsd.&nbsp;</p>', '1568256618.png', '2019-09-12 09:50:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `company`
--

CREATE TABLE IF NOT EXISTS `company` (
`id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `profil` text NOT NULL,
  `foto` text NOT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `company`
--

INSERT INTO `company` (`id`, `nama`, `profil`, `foto`, `created`) VALUES
(1, 'Badan Penanggulangan Bencana Aceh', '<p>tsatin sdfosdf sdfj sdkfj sdfsdf. dskfjs dfk sdfsdf</p>\r\n<p><img src="../asset/kcfinder/upload/image/test.png" alt="testing" width="99%" /></p>\r\n<p>&nbsp;</p>\r\n<p>dsfsd</p>\r\n<p><img src="../asset/kcfinder/upload/image/cara-install-composer-di-windows/gambar2.png" alt="" width="99%" /></p>', '1568134570.png', '2019-09-10 23:58:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `desa`
--

CREATE TABLE IF NOT EXISTS `desa` (
`id` int(11) NOT NULL,
  `kddesa` varchar(11) DEFAULT NULL,
  `nmdesa` varchar(100) DEFAULT NULL,
  `kec_id` int(11) DEFAULT NULL,
  `kab_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `desa`
--

INSERT INTO `desa` (`id`, `kddesa`, `nmdesa`, `kec_id`, `kab_id`) VALUES
(1, 'DESA-00001', 'Keude Siblah', 1, 1),
(2, 'DESA-00002', 'Meudang Ara', 1, 1),
(3, 'DESA-00003', 'Desa Kedai', 2, 1),
(4, 'DESA-00004', 'Kampong Padang', 2, 1),
(5, 'DESA-00005', 'Padang Hilir', 2, 1),
(6, 'DESA-00006', 'Panton Labu', 3, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_bencana`
--

CREATE TABLE IF NOT EXISTS `jenis_bencana` (
`id` int(11) NOT NULL,
  `kdjenisb` varchar(11) DEFAULT NULL,
  `nmjenisb` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `jenis_bencana`
--

INSERT INTO `jenis_bencana` (`id`, `kdjenisb`, `nmjenisb`) VALUES
(1, 'JB-00001', 'Banjir'),
(2, 'JB-00002', 'Kebakaran'),
(3, 'JB-00003', 'Tsunami'),
(4, 'JB-00004', 'Erupsi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupaten`
--

CREATE TABLE IF NOT EXISTS `kabupaten` (
`id` int(11) NOT NULL,
  `kdkab` varchar(11) DEFAULT NULL,
  `nmkab` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kabupaten`
--

INSERT INTO `kabupaten` (`id`, `kdkab`, `nmkab`) VALUES
(1, 'KAB-00001', 'Aceh Barat Daya'),
(2, 'KAB-00002', 'Aceh Selatan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE IF NOT EXISTS `kecamatan` (
`id` int(11) NOT NULL,
  `kdkec` varchar(11) DEFAULT NULL,
  `nmkec` varchar(100) DEFAULT NULL,
  `kab_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `kdkec`, `nmkec`, `kab_id`) VALUES
(1, 'KEC-00001', 'Blangpidie', 1),
(2, 'KEC-00002', 'Manggeng', 1),
(3, 'KEC-00003', 'Tapaktuan', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `korban`
--

CREATE TABLE IF NOT EXISTS `korban` (
`id` int(11) NOT NULL,
  `kdkorban` varchar(12) DEFAULT NULL,
  `korban` varchar(255) DEFAULT NULL,
  `nmjenisb` int(11) DEFAULT NULL COMMENT 'jenisbencanaid',
  `nmkab` int(11) DEFAULT NULL COMMENT 'kabupatenid',
  `nmkec` int(11) DEFAULT NULL COMMENT 'kecamatanid',
  `nmdesa` int(11) DEFAULT NULL COMMENT 'desaid',
  `tanggal` date DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `korban`
--

INSERT INTO `korban` (`id`, `kdkorban`, `korban`, `nmjenisb`, `nmkab`, `nmkec`, `nmdesa`, `tanggal`, `tahun`) VALUES
(1, 'K-00001', '88', 2, 1, 1, 2, '2018-06-05', 2018),
(2, 'K-00002', '56', 1, 1, 1, 1, '2019-09-10', 2019),
(4, 'K-00003', '76', 1, 1, 1, 2, '2019-09-12', 2017),
(5, 'K-00004', '199', 3, 2, 3, 6, '2019-09-12', 2017),
(6, 'K-00005', '12', 1, 1, 1, 1, '2019-09-12', 2017);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `nama` varchar(50) CHARACTER SET utf8 NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password_hid` varchar(255) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `password_hid`, `level`, `created`) VALUES
(1, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'purnandi', 'admin', '2019-03-26 20:28:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
 ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `desa`
--
ALTER TABLE `desa`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_bencana`
--
ALTER TABLE `jenis_bencana`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `korban`
--
ALTER TABLE `korban`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `desa`
--
ALTER TABLE `desa`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `jenis_bencana`
--
ALTER TABLE `jenis_bencana`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kabupaten`
--
ALTER TABLE `kabupaten`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `korban`
--
ALTER TABLE `korban`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
