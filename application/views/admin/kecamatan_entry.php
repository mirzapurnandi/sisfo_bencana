<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Data Kecamatan</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?= site_url('admin/kabupaten')?>">Data Kecamatan</a></li>
			<li class="active"><?= $tombol; ?> Kecamatan</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?= $tombol; ?> Data</h3>
					</div>
					<form class="form-horizontal" id="frmkecamatan" action="<?=site_url('Admin_kecamatan/proses'); ?>" method="post">
						<input type="hidden" name="kecamatanid" value="<?= $kecamatanid ?>">
						<div class="box-body">
							<div class="form-group">
								<label for="kabupaten_id" class="col-sm-2 control-label">Kabupaten <font color="red">*</font></label>
								<div class="col-sm-10">
									<?= form_dropdown('kabupatenid', $dropdown_kabupaten, $kabupatenid, 'id="kabupatenid" class="form-control" required="required"'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="fasilitas" class="col-sm-2 control-label">Nama Kecamatan <font color="red">*</font></label>
								<div class="col-sm-10">
									<input type="text" name="nama" id="nama" value="<?= $nama ?>" class="form-control" required="required">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-info pull-left kamar"><?= $tombol; ?></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>