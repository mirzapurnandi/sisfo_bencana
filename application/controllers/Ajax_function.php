<?php
	if(!defined('BASEPATH'))
		exit('No direct access allowed !');
		
	class Ajax_function extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('global_model');
		}

		function cari_kecamatan(){
			$kabupatenid = $_POST['kabupatenid'];
			
			$result = $this->global_model->get('kecamatan', '*', array('kab_id'=>$kabupatenid));
			$data = "<option value=''> == Pilih Kecamatan == </option>";
			foreach($result as $key => $val){
				$data .= "<option value='".$val['id']."'>".$val['nmkec']."</option>";
			}
			echo $data;
		}

		function cari_desa(){
			$kecamatanid = $_POST['kecamatanid'];
			
			$result = $this->global_model->get('desa', '*', array('kec_id'=>$kecamatanid));
			$data = "<option value=''> == Pilih Desa == </option>";
			foreach($result as $key => $val){
				$data .= "<option value='".$val['id']."'>".$val['nmdesa']."</option>";
			}
			echo $data;
		}
	}
?>