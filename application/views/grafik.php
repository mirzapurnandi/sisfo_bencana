<div class="content-wrapper">
	<div class="container">

		<section class="content-header">
			<h1>
				Grafik
				<small><?= setting()->nama; ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="<?= config_item('base_url')?>"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Grafik</li>
			</ol>
		</section>

		<section class="content">
			<div class="box box-default">
				<div class="box-body">
					<div class="col-md-12">
						<div class="box box-solid">
							<!-- /.box-header -->
							<div class="box-body">
								<div id="grafik_bencana" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>

			</div>

		</section>

	</div>

</div>