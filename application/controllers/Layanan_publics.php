<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Layanan_publics extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('umum');
		}

		function index(){
			$bencanaid 		= $this->input->get('bencanaid');
			$kabupatenid 	= $this->input->get('kabupatenid');
			$kecamatanid 	= $this->input->get('kecamatanid');
			$desaid 		= $this->input->get('desaid');

			$this->template->add_js('asset/js/desa.js?'.config_item('versi'));

			$data['result'] = $this->umum->get_korban("", $bencanaid, $kabupatenid, $kecamatanid, $desaid);
			

			$data['dropdown_kabupaten'] = dropdown_kabupaten();
			$data['dropdown_bencana'] 	= dropdown_bencana();
			$data['dropdown_kecamatan'] = array('== Pilih Kecamatan ==');
			$data['dropdown_desa'] 		= array('== Pilih Desa ==');

			$data['bencanaid'] 		= $bencanaid;
			$data['kabupatenid'] 	= $kabupatenid;
			$data['kecamatanid'] 	= $kecamatanid;
			$data['desaid'] 		= $desaid;

			$this->template->view('template', 'layanan_public', $data);
		}

		function entry($id = ""){
			$this->template->add_js('asset/js/desa.js?'.config_item('versi'));
			$arr_tahun[''] = "== Pilih Tahun ==";
			for($i=date('Y'); $i>=1980; $i--){
				$arr_tahun[$i] = $i;
			}
			$data['dropdown_tahun'] = $arr_tahun;

			$data['dropdown_kabupaten'] = dropdown_kabupaten();
			$data['dropdown_bencana'] 	= dropdown_bencana();

			$data['korbanid'] 	 = '';
			$data['dropdown_kecamatan'] = array('== Pilih Kecamatan ==');
			$data['dropdown_desa'] 		= array('== Pilih Desa ==');
			$data['desaid'] 	 = 0;
			$data['kecamatanid'] = 0;
			$data['kabupatenid'] = 0;
			$data['bencanaid'] 	 = 0;
			$data['tombol'] 	 = 'Tambah';
			$data['jumlah_korban'] = '';
			$data['tanggal'] 	 = date("Y-m-d");
			$data['tahun'] 		 = '';

			$this->template->view('template', 'layanan_public_entry', $data);
		}

		function proses(){
			$this->db->trans_start();
			$korbanid = $this->input->post('korbanid') ? $this->input->post('korbanid') : "";

			$bencanaid		= $this->input->post('bencanaid');
			$jumlah_korban	= $this->input->post('jumlah_korban');
			$tanggal		= $this->input->post('tanggal');
			$tahun			= $this->input->post('tahun');
			$kabupatenid 	= $this->input->post('kabupatenid');
			$kecamatanid 	= $this->input->post('kecamatanid');
			$desaid 		= $this->input->post('desaid');

			$return_data = array(
				'korban' 	=> $jumlah_korban,
				'nmjenisb'	=> $bencanaid,
				'nmkab'		=> $kabupatenid,
				'nmkec'		=> $kecamatanid,
				'nmdesa'	=> $desaid,
				'tanggal'	=> $tanggal,
				'tahun'		=> $tahun
			);

			$return_data += array(
				'kdkorban' => $this->global_model->getkodeunik("korban", "kdkorban", "K-")
			);
			$this->global_model->insert('korban', $return_data);
			$this->db->trans_complete();
			$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");

			redirect('layanan_publics');
		}
	}
?>